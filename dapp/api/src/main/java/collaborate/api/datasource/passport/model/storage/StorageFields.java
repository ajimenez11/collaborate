package collaborate.api.datasource.passport.model.storage;

public class StorageFields {

  private StorageFields() {
  }

  public static final String MULTISIGS = "multisigs";
  public static final String NFT_INDEXER = "nft_indexer";
  public static final String TOKEN_METADATA = "token_metadata";
  public static final String TOKEN_ID_BY_ASSET_ID = "token_id_by_asset_id";

}
